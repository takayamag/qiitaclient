package sample.qiitaclient.dagger

/**
 * Created by tak on 2017/08/13.
 */

import dagger.Component
import sample.qiitaclient.MainActivity
import javax.inject.Singleton

@Component(modules = arrayOf(ClientModule::class))
@Singleton
interface AppComponent {
    fun inject(mainActivity: MainActivity)
}