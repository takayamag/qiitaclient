package sample.qiitaclient

/**
 * Created by tak on 2017/08/13.
 */

import android.app.Application
import sample.qiitaclient.dagger.AppComponent
import sample.qiitaclient.dagger.DaggerAppComponent

class QiitaClientApp: Application() {

    val component: AppComponent by lazy {
        DaggerAppComponent.create()
    }
}