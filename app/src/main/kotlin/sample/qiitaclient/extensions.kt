package sample.qiitaclient

/**
 * Created by tak on 2017/08/09.
 */

import android.content.Context
import android.databinding.BindingAdapter
import android.webkit.WebView
import android.widget.ImageView
import android.widget.Toast
import com.bumptech.glide.Glide

fun Context.toast(message: String, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

@BindingAdapter("bind:imageUrl")
fun ImageView.loadImage(url: String) {
    Glide.with(context).load(url).into(this)
}

@Suppress("EXTENSION_SHADOWED_BY_MEMBER")
@BindingAdapter("bind:loadUrl")
fun WebView.loadUrl(url: String) {
    loadUrl(url)
}