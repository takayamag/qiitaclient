package sample.qiitaclient.view

/**
 * Created by tak on 2017/08/09.
 */

import android.content.Context
import android.databinding.BindingMethod
import android.databinding.BindingMethods
import android.databinding.DataBindingUtil
import android.util.AttributeSet
import android.view.LayoutInflater
import android.widget.FrameLayout
import sample.qiitaclient.R
import sample.qiitaclient.databinding.ViewArticleBinding
import sample.qiitaclient.model.Article

@BindingMethods(BindingMethod(type = Article::class,
        attribute = "bind:article",
        method = "setArticle"))
class ArticleView : FrameLayout {

    constructor(context: Context?) : super(context)

    constructor(context: Context?,
                attrs: AttributeSet?) : super(context, attrs)

    constructor(context: Context?,
                attrs: AttributeSet?,
                defStyleAttr: Int) : super(context, attrs, defStyleAttr)

    constructor(context: Context?,
                attrs: AttributeSet?,
                defStyleAttr: Int,
                defStyleRes: Int) : super(context, attrs, defStyleAttr, defStyleRes)

    private val binding: ViewArticleBinding =
            DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_article, this, true)

    fun setArticle(article: Article) {
        binding.article = article
    }
}
